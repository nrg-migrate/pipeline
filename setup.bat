@echo off
rem ---------------------------------------------------------------------------
rem Setup processing for
rem
rem Environment Variable Prequisites
rem
rem   JAVA_HOME       Directory of Java Version
rem
rem
rem $Id: setup.bat,v 1.1 2008/11/13 16:23:07 mohanar Exp $
rem ---------------------------------------------------------------------------


if "%1"=="" goto :usage
if "%2"=="" goto :usage

set PATH=%JAVA_HOME%\bin;%PATH%

:continue

echo SETIING UP PIPELINE UTILITIES

echo Using JAVA_HOME:           %JAVA_HOME%
echo .
echo Verify java version (with 'java -version')
call java -version

set WORK_DIR=%CD%

set MAVEN_HOME=%WORK_DIR%\maven-1.0.2
if NOT exist %MAVEN_HOME%  set WORK_DIR=%CD%\pipeline

set MAVEN_HOME=%WORK_DIR%\maven-1.0.2

echo %WORK_DIR%
echo %MAVEN_HOME%

:EXECUTE

rem EXECUTE MAVEN Setup

%MAVEN_HOME%\bin\maven -d %WORK_DIR% "-Dadmin.email=%1" "-Dsmtp.server=%2" pipeline:setup 

goto :END

:USAGE

echo Usage: setup.bat admin_email smtp_server 

:END

