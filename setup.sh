#!/bin/sh 
# ---------------------------------------------------------------------------
# Setup processing for
#
# Environment Variable Prequisites
#
#   JAVA_HOME       Directory of Java Version
#
#
# $Id: setup.sh,v 1.1 2008/11/13 16:23:07 mohanar Exp $
# ---------------------------------------------------------------------------

echo 

if [ "$#" -lt "2" ]; then
  echo "Missing required command line arguments"
  echo "USAGE: $0 <admin email id> <SMTP server>"
  exit 1
fi


echo " "
echo "Using JAVA_HOME:         $JAVA_HOME"
echo " "
echo "Verify Java Version (with java -version)"
java -version


WORK_DIR=`pwd`

MAVEN_HOME="$WORK_DIR"/maven-1.0.2
if [ ! -d  $MAVEN_HOME ];  then 
  WORK_DIR="$WORK_DIR"/pipeline
fi 

MAVEN_HOME="$WORK_DIR"/maven-1.0.2

echo $WORK_DIR
echo $MAVEN_HOME

chmod +x "$MAVEN_HOME"/bin/maven

"$MAVEN_HOME"/bin/maven  -d $WORK_DIR -Dadmin.email=$1 -Dsmtp.server=$2  pipeline:setup 

exit $status



